#include <string>
#include <iostream>

#define CHARACTER argv[curr_argv][i]

int	main(int argc, char **argv)
{
	if (argc < 2)
	{
		std::cout<<"* LOUD AND UNBEARABLE FEEDBACK NOISE *"<<std::endl;
	}
	else
	{
		int	curr_argv = 0;

		while (++curr_argv < argc)
		{
			for (unsigned long i(0); i < ((std::string)argv[curr_argv]).length(); i++)
				std::cout<<(char)toupper(CHARACTER);
		}
			std::cout<<"\n";
	}
	return (0);
}