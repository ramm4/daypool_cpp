#include "Member.hpp"

#define FIXCIN std::cin.clear(); std::cin.ignore(10000, '\n');

bool	Member::fully(void)
{
	if (first_name.empty())
		return (false);
	return (true);
}

void	Member::short_info(int user_index)
{
	std::string	result;
	std::string tmp;
	std::string *show_data[4];
	std::string str_index = std::to_string(user_index);

	show_data[0] = &str_index;
	show_data[1] = &first_name;
	show_data[2] = &last_name;
	show_data[3] = &nick;

	for (int i(0); i < 4; ++i)
	{
		std::cout.width(10);
		if((show_data[i])->length() > 10)
			tmp = (show_data[i])->substr(0, 10).replace(9, 1, ".");
		else
			tmp.assign(*show_data[i]);
		std::cout<<std::right<<tmp<<" |";
	}
	std::cout<<"\n";
}

void	Member::full_info(void)
{
	std::cout << "First Name: " << first_name << std::endl;
	std::cout << "Last Name: " << last_name << std::endl;
	std::cout << "Nickname: " << nick << std::endl;
	std::cout << "Login: " << login << std::endl;
	std::cout << "Address: " << postal_adrr << std::endl;
	std::cout << "Email: " << email_adrr << std::endl;
	std::cout << "Phone: " << phone << std::endl;
	std::cout << "Birthday: " << birthday_day << "/" << birthday_year << "/" << birthday_month << std::endl;
	std::cout << "Favorite Meal: " << favorite_meal << std::endl;
	std::cout << "Underwear Color: " << underwear_color << std::endl;
	std::cout << "Secret: " << darkest_secter << std::endl;
}

void	Member::add(void)
{
	std::cout<< "First Name: "; std::getline(std::cin, first_name);
	std::cout<< "Last Name: "; std::getline(std::cin, last_name);
	std::cout<< "Nickname: "; std::getline(std::cin, nick);
	std::cout<< "Login: "; std::getline(std::cin, login);
	std::cout<< "Address: "; std::getline(std::cin, postal_adrr);

	while (true)
	{
		std::cout<< "Email: "; std::getline(std::cin, email_adrr);
		if (email_adrr.find('@') != std::string::npos &&
						email_adrr.find('.')!= std::string::npos)
			break;
	}

	while (true)
	{
		std::cout<< "Phone: "; std::getline(std::cin, phone);
		if (phone.find_first_not_of("0123456789") == std::string::npos)
			break;
	}

	do {
		std::cout << "Birthday Year (YYYY): ";
		if (!(std::cin >> birthday_year)) FIXCIN;
	} while (birthday_year < 1800 || birthday_year > 2019);

	do {
		std::cout << "Birthday Month (MM): ";
		if (!(std::cin >> birthday_month)) FIXCIN;
	} while (birthday_month < 1 || birthday_month > 12);

	do {
		std::cout << "Birthday Day (DD): ";
		if (!(std::cin >> birthday_day)) FIXCIN;
	} while (birthday_day < 1 || birthday_day > 31);

	std::cout << "Favorite Meal: "; std::getline(std::cin, favorite_meal);
	std::cout << "Underwear Color: "; std::getline(std::cin, underwear_color);
	std::cout << "Darkest Secret: "; std::getline(std::cin, darkest_secter);
	std::cout << "Member is added"<<std::endl;
}
