#ifndef _MEMBER_H_
# define _MEMBER_H_
	
#include <iostream>
#include <iomanip>
#include <string>

class Member
{
	private:
		std::string first_name;
		std::string last_name;
		std::string nick;
		std::string login;
		std::string postal_adrr;
		std::string email_adrr;
		std::string phone;
		std::string favorite_meal;
		std::string underwear_color;
		std::string darkest_secter;

		short 		birthday_month;
		int			birthday_year;
		short 		birthday_day;

	public:
		void		add(void);
		void		short_info(int user_index);
		void		full_info(void);
		bool		fully(void);

};

#endif