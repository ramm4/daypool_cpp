#include "Member.hpp"

#define FIXCIN std::cin.clear();

#define	DATABASE_SIZE 8

void	table(void)
{
	for(int i(0); i < 12; i++)
		std::cout<<"____";
	std::cout<<"\n";
}

void	display_members(Member *users, int &suffuring)
{
	table();
	std::cout << "     Index |First Name | Last Name |  Nickname |" << std::endl;

	int			index = 0;

	while (index < suffuring)
	{
		users[index].short_info(index);
		++index;
	}

	table();

	index = -1;

	std::cout << "If you want to get more detail about member write a necessary index." << std::endl;
	while(std::cin >> index)
	{
		std::cout << "If you want to get more detail about member write a necessary index." << std::endl;
	 	FIXCIN;
	 	if (index >= 0 && index < suffuring)
			users[index].full_info();
	}
	FIXCIN;
}

int main(void)
{

	Member		users[8];
	int			suffuring = 0;
	std::string cmmnd;

	std::cout<< "Available command: ADD | SEARCH | EXIT"<<std::endl;

	do
	{
		std::getline(std::cin, cmmnd);

		if (cmmnd == "ADD")
		{
			if (suffuring < DATABASE_SIZE)
			{
				users[suffuring].add();
				++suffuring;
			}
			else
				std::cout<< "Limited save: Buy PRO version -> [JUST 10 $]"<<std::endl;
		}
		else if (cmmnd == "SEARCH")
		{
			if(!users[0].fully())
				std::cout<<"Database empty()"<<std::endl;
			else
				display_members(&users[0], suffuring);
		}
		else if (cmmnd != "EXIT")
		{
			std::cout<<"Unexpected command: "<<cmmnd<<std::endl;
		}

	} while (cmmnd != "EXIT");

}