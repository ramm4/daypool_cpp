#include <iostream>
#include "Pony.hpp"

Pony::Pony(std::string _name, std::string _color, int _age) :
			name(_name), color(_color), age(_age)
{
	std::cout<< "My name is " <<name<< std::endl;
	std::cout<<age<<" years ago, I was born"<<std::endl;
	return;
}

Pony::~Pony(void)
{
	std::cout << name << " said, Goodbye, I go home" << std::endl;
	return;
}