#ifndef _PONY_HPP_
#define _PONY_HPP_

#include <string>

class Pony
{
private:
	std::string	name;
	std::string	color;
	int			age;

public:
	Pony(std::string _name, std::string _color, int _age);
	~Pony(void);
};

#endif