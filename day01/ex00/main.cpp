#include "Pony.hpp"

void	ponyOnTheHeap(void)
{
	Pony *pony = new Pony("HEAP", "Blue", 56);
	delete pony;
}

void	ponyOnTheStack(void)
{
	Pony pony = Pony("STACK", "RED", 78);
}

int 	main()
{
	ponyOnTheHeap();
	ponyOnTheStack();
	return (0);
}