#include <iostream>

#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent(void)
{
	_type = "default";
}

ZombieEvent::~ZombieEvent(void) { }

void	ZombieEvent::setZombieType(std::string type)
{
	_type = type;
};

Zombie*	ZombieEvent::newZombie(std::string name)
{
	Zombie* zombie = new Zombie(name, _type);
	return (zombie);
}