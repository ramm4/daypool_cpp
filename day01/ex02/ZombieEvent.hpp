#ifndef _ZOMBIEEVENT_HPP_
#define _ZOMBIEEVENT_HPP_

#include "Zombie.hpp"

class	ZombieEvent
{

private:
	std::string	_type;

public:
	ZombieEvent(void);
	~ZombieEvent(void);

	void	setZombieType(std::string type);
	Zombie*	newZombie(std::string name);
};

#endif