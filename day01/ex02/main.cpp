#include <stdlib.h>
#include <time.h>

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

void	randomChump(void)
{
	std::string name[8] = {"Bobo", "Zoro", "Zaz", "Heavy", "Eddy", "Alkash", "Pet6ka", "Billy"};
	Zombie zombie = Zombie(name[rand() % 8], "random");
	zombie.announce();
	return;
}

int	main()
{

	ZombieEvent event = ZombieEvent();
	Zombie* bill = event.newZombie("Bill");
	event.setZombieType("raging");
	Zombie* dave = event.newZombie("Dave");
	delete bill;
	delete dave;
	
	srand(time(NULL));
	randomChump();
	randomChump();
	randomChump();
	return (0);
}