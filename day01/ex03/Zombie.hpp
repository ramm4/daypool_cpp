#ifndef _ZOMBIE_HPP_
#define _ZOMBIE_HPP_

#include <iostream>

class Zombie
{

private:
	std::string	_name;
	std::string	_type;

public:
	Zombie(std::string name, std::string type);
	Zombie();
	~Zombie(void);

	void	announce(void);
};

#endif