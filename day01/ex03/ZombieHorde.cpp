#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n) : _num(n)
{
	_zombies = new Zombie[n]();
	srand(time(NULL));
	for (int i = 0; i < n; i++)
		_zombies[i] = generate_zombik();
}

ZombieHorde::~ZombieHorde(void)
{
	delete [] _zombies;
}

Zombie::Zombie(){};

void	ZombieHorde::announce(void)
{
	for (int i = 0; i < _num; i++)
		_zombies[i].announce();
}

Zombie&	ZombieHorde::generate_zombik(void)
{
	std::string name[8] = {"Bobo", "Zoro", "Zaz", "Heavy", "Eddy", "Alkash", "Pet6ka", "Billy"};
	Zombie *zombie = new Zombie(name[rand() % 8], "PIPKA");
	return (*zombie);
}