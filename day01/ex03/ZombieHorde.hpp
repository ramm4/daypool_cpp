#ifndef _ZOMBIEHORDE_HPP_
# define _ZOMBIEHORDE_HPP_

#include "Zombie.hpp"

class	ZombieHorde
{

private:
	int		_num;
	Zombie 	*_zombies;
public:
	ZombieHorde(int n);
	~ZombieHorde(void);

	Zombie	&generate_zombik(void);
	void	announce(void);
};

#endif