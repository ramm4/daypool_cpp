#include <iostream>

int		main()
{
	std::string	brain = "HI THIS IS BRAIN";
	std::string	*ptr = &brain;
	std::string	&ref = brain;

	std::cout << "pointer to " << *ptr << std::endl;
	std::cout << "reference to " << &ref << std::endl;
	return (0);
}