#ifndef _BRAIN_HPP_
#define _BRAIN_HPP_

#include <string>

class Brain
{
	public:
		Brain(void);
		~Brain(void);
		std::string	identify(void);
};

#endif