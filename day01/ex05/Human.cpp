#include "Human.hpp"

Human::Human(void)
{
	_brain = new Brain();
};

Human::~Human(void)
{
	delete _brain;
};

std::string	Human::identify(void)
{
	return (_brain->identify());
}

Brain 		&Human::getBrain(void)
{
	return (*_brain);
}