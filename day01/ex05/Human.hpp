#ifndef _HUMAN_HPP_
#define _HUMAN_HPP_

#include "Brain.hpp"

class	Human
{
private:
	Brain*	_brain;

public:
	Human(void);
	~Human(void);

	std::string	identify(void);
	Brain 		&getBrain(void);
};

#endif