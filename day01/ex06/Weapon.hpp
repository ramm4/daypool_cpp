#ifndef _WEAPON_HPP_
#define _WEAPON_HPP_

#include <string>

class	Weapon
{
private:
	std::string	_type;

public:
	Weapon(std::string weapon);
	~Weapon(void);

	std::string	getType(void) const;
	void		setType(std::string str);
};

#endif