#include <iostream>
#include <fstream>

int	main(int ac, char **av)
{
	if (ac == 4)
	{
		std::string	name = av[1];
		std::string old = av[2];
		std::string	new_word = av[3];


		if (old == new_word)
		{
			std::cout << name << " Change Doesn't Matter" << std::endl;
			return (0);
		}

		std::ifstream is(name);
		if (!is)
		{
			std::cout << name << " does not exist" << std::endl;
			return (1);
		}
		else
		{
			std::ofstream os(name + ".replace");

			if (!os) return (1);
		
			std::string	s;
			while (std::getline(is, s))
			{
				for (size_t p = s.find(old); p != std::string::npos; p = s.find(old, p))
					s.replace(p, old.length(), new_word);
				os << s << std::endl;
			}
		}
	}
	else
		std::cout << "usage: ./replace [file_name] string_1 string_2" << std::endl;
	return (0);
}