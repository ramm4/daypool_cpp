#include "Human.hpp"

int main()
{
	Human human = Human();
	human.action("meleeAttack", "Hit to stomach");
	human.action("rangedAttack", "Shot");
	human.action("intimidatingShout", "Jon seems,");
	human.action("invalid", "hello");
	return (0);
}