#ifndef _FIXED_HPP_
#define _FIXED_HPP_

class	Fixed
{
private:
	int			_fpv;
	static int	_fbits;

public:
	Fixed(void);
	Fixed(const Fixed &old);
	Fixed& operator = (const Fixed &old);
	~Fixed(void);

	int		getRawBits(void) const;
	void	setRawBits(int const raw);
};

#endif