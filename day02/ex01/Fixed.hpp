#ifndef _FIXED_HPP_
#define _FIXED_HPP_

class	Fixed
{
private:
	int			_fpv;
	static int	_fbits;

public:
	Fixed(void);
	Fixed(const Fixed &old);
	Fixed& operator = (const Fixed &old);
	~Fixed(void);

	Fixed(const int fpv);
	Fixed(const float fpv);

	int		getRawBits(void) const;
	void	setRawBits(int const raw);
	float	toFloat(void) const;
	int		toInt(void) const;
};

std::ostream &operator << (std::ostream &output, Fixed const &old);

#endif