#ifndef _FIXED_HPP_
#define _FIXED_HPP_

class	Fixed
{
private:
	int			_fpv;
	static int	_fbits;

public:
	Fixed(void);
	Fixed(const Fixed &old);
	~Fixed(void);
	Fixed(const int fpv);
	Fixed(const float fpv);

	Fixed&	operator = (const Fixed &old);

	bool	operator > (const Fixed &old) const;
	bool	operator < (const Fixed &old) const;
	bool	operator >=(const Fixed &old) const;
	bool	operator <=(const Fixed &old) const;
	bool	operator ==(const Fixed &old) const;
	bool	operator !=(const Fixed &old) const;

	Fixed	operator + (const Fixed &old) const;
	Fixed	operator - (const Fixed &old) const;
	Fixed	operator * (const Fixed &old) const;
	Fixed	operator / (const Fixed &old) const;

	Fixed&	operator ++(void);
	Fixed	operator ++(int);
	Fixed&	operator --(void);
	Fixed	operator --(int);

	int		getRawBits(void) const;
	void	setRawBits(int const raw);
	float	toFloat(void) const;
	int		toInt(void) const;
};

std::ostream	&operator << (std::ostream &output, Fixed const &old);
Fixed&			min(Fixed &a, Fixed &b);
Fixed&			max(Fixed &a, Fixed &b);
const Fixed&	min(const Fixed &a, const Fixed &b);
const Fixed&	max(const Fixed &a, const Fixed &b);
#endif