#ifndef _BIND_HPP_
#define _BIND_HPP_

#include <iostream>
#include <string>
#include <cstdlib>
#include "Fixed.hpp"

void	checkInput(char *s);
char	*cleanSpaces(char *str);
Fixed	parseFactor(char **x);
Fixed	parseProduct(char **x);
Fixed	parseSum(char **x);
Fixed	eval_expr(char *expr);

#endif