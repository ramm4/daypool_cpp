#include "bind.hpp"

static Fixed	getFixed(char **x)
{
	Fixed num = std::atof(*x);
	while (isdigit(**x) || **x == '.')
		(*x)++;
	return (num);
}

Fixed	eval_expr(char *expr)
{
	Fixed	calc;

	calc = parseSum(&expr);
	return (calc);
}

void	checkInput(char *s)
{
	bool	inv = false;
	int		brackets = 0;

	for (size_t i = 0;i < strlen(s);i++)
	{
		if (!isdigit(s[i]) && !isspace(s[i]) &&
				s[i] != '.' && s[i] != '+' && s[i] != '-' && s[i] != '*' && s[i] != '/')
		{
			if (s[i] == '(')
				brackets++;
			else if (s[i] == ')')
				brackets--;
			else
				inv = true;
		}
		if (brackets < 0)
			inv = true;
	}
	if (inv)
	{
		std::cout<<"Invalid Expression"<<std::endl;
		exit(1);
	}
}

char	*cleanSpaces(char *str)
{
	int	c = 0;

	for (int i = 0;str[i];i++)
		if (!isspace(str[i]))
			str[c++] = str[i];
	str[c] = '\0';
	return (str);
}

Fixed	parseFactor(char **x)
{
	Fixed	num = 0;

	if (isdigit(**x) || **x == '-')
		num = getFixed(x);
	else if (**x == '(')
	{
		(*x)++;
		num = parseSum(x);
		if (**x == ')')
			(*x)++;
	}
	else
	{
		std::cout<<"Invalid Expression"<<std::endl;
		exit(1);
	}
	return (num);
}

Fixed	parseProduct(char **x)
{
	char	sign;

	Fixed	a = parseFactor(x);
	while (**x == '*' || **x == '/')
	{
		sign = **x;
		(*x)++;
		Fixed	b = parseFactor(x);
		if (sign == '*')
			a = a * b;
		else if (sign == '/')
		{
			if (b == 0)
			{
				std::cout<<"Error: cannot divide by 0"<<std::endl;
				exit(1);
			}
			a = a / b;
		}
	}
	return (a);
}

Fixed	parseSum(char **x)
{
	Fixed	a = parseProduct(x);
	while (**x == '+' || **x == '-')
	{
		char sign = **x;
		(*x)++;
		Fixed	b = parseProduct(x);
		if (sign =='+')
			a = a + b;
		else if (sign == '-')
			a = a - b;
	}
	return (a);
}