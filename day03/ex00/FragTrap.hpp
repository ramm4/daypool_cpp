#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <iostream>

class FragTrap
{

private:
	std::string _Name;
	int Max_hit_points;
	int Max_energy_points;
	int Ranged_attack_damage;
	int Melee_attack_damage;
	int Hit_points;
	int Energy_points;
	int Level;
	int Armor_damage_reduction;

public:
	FragTrap();
	FragTrap(std::string const & Name);
	~FragTrap();
	FragTrap(FragTrap const &src);
	FragTrap & operator=(FragTrap const &src);
	
	std::string		get_Name() const;
	int		getMax_hit_points() const;;
	int		getMax_energy_points() const;
	int		getRanged_attack_damage() const;
	int		getMelee_attack_damage() const;
	int		getHitPoints() const;
	int		getEnergyPoints() const;
	int		getLevel() const;
	int 	getArmorDamageReduction() const;

	void	setHitPoints(unsigned int amount);
	void	setEnergyPoints(unsigned int amount);

	void	rangedAttack(std::string const & target);
	void	meleeAttack(std::string const & target);

	void	trololo(std::string const & target);
	void	unicorn(std::string const & target);
	void	pony(std::string const & target);



	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);


	void	variant( std::string const & target );
	int		random_int_in_range( void );
	void	vaulthunter_dot_exe(std::string const & target);
	
};

































































#endif
