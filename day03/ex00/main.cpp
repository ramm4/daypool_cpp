#include "FragTrap.hpp"

int		main(void)
{
	FragTrap Destroyer("Destroyer");

	// string destruction;
	std::cout << "\033[1;33mAttacks:\033[0m" << std::endl;
	
	Destroyer.rangedAttack("target");
		Destroyer.meleeAttack("target");

	// actual damage;
	std::cout << "\033[1;33mDamage func:\033[0m" << std::endl;
	
	Destroyer.takeDamage(-10000);
		Destroyer.takeDamage(20);
			Destroyer.takeDamage(10);
				Destroyer.takeDamage(1000);

	// healing section;
	std::cout << "\033[1;33mHealing:\033[0m" << std::endl;
	
	Destroyer.beRepaired(0);	
		Destroyer.beRepaired(1);	
			Destroyer.beRepaired(100000000);	


	// vaulthunter_dot_exe;
	std::cout << "\033[1;33mHunter:\033[0m" << std::endl;

	Destroyer.vaulthunter_dot_exe("HamBurger");	
		Destroyer.vaulthunter_dot_exe("CheeseBurger");
			Destroyer.vaulthunter_dot_exe("DoubleCheeseBurger");
				Destroyer.vaulthunter_dot_exe("Maffin");	
					Destroyer.vaulthunter_dot_exe("Rozshjok :)");	
						Destroyer.vaulthunter_dot_exe("HamBurger");	
							Destroyer.vaulthunter_dot_exe("CheeseBurger");
								Destroyer.vaulthunter_dot_exe("DoubleCheeseBurger");
									Destroyer.vaulthunter_dot_exe("Maffin");	
										Destroyer.vaulthunter_dot_exe("Rozshjok :)");	

	FragTrap Unicorn("Unicorn");
	FragTrap Killer("Killer");
	FragTrap Pony(Killer);
	Killer = Pony;

	return (0);
}