#include "FragTrap.hpp"

FragTrap::FragTrap() {}

FragTrap::~FragTrap()
{
	std::cout << "\033[1;33mDestruction:\033[0m" << std::endl;
	std::cout << "Destructed bot with Name -> " << _Name << std::endl;
	std::cout << "Destructed bot with Max_hit_points -> " << Max_hit_points << std::endl;
	std::cout << "Destructed bot with Hit_points -> " << Hit_points << std::endl;
	std::cout << "Destructed bot with Max_energy_points -> " << Max_energy_points << std::endl;
	std::cout << "Destructed bot with Ranged_attack_damage -> " << Ranged_attack_damage << std::endl;
	std::cout << "Destructed bot with Melee_attack_damage -> " << Melee_attack_damage << std::endl;
	std::cout << "Destructed bot with Energy_points -> " << Energy_points << std::endl;
	std::cout << "Destructed bot with Level -> " << Level << std::endl;
	std::cout << "Destructed bot with Armor_damage_reduction -> " << Armor_damage_reduction << std::endl;
}

FragTrap::FragTrap(std::string const & Name): _Name(Name) 
{
	std::cout << "\033[1;33mConstruction:\033[0m" << std::endl;
	this->Max_hit_points = 100;
	this->Hit_points = 100;
	this->Max_energy_points = 100;
	this->Ranged_attack_damage = 20;
	this->Melee_attack_damage = 30;
	this->Energy_points = 100;
	this->Level = 1;
	this->Armor_damage_reduction = 5;

	std::cout << "Created bot with Name -> " << _Name << std::endl;
	std::cout << "Created bot with Max_hit_points -> " << Max_hit_points << std::endl;
	std::cout << "Created bot with Hit_points -> " << Hit_points << std::endl;
	std::cout << "Created bot with Max_energy_points -> " << Max_energy_points << std::endl;
	std::cout << "Created bot with Ranged_attack_damage -> " << Ranged_attack_damage << std::endl;
	std::cout << "Created bot with Melee_attack_damage -> " << Melee_attack_damage << std::endl;
	std::cout << "Created bot with Energy_points -> " << Energy_points << std::endl;
	std::cout << "Created bot with Level -> " << Level << std::endl;
	std::cout << "Created bot with Armor_damage_reduction -> " << Armor_damage_reduction << std::endl;
}

FragTrap::FragTrap(FragTrap const &src) 
{
    std::cout << "Copy Constructor called" << std::endl;
    *this = src;
}
FragTrap &FragTrap::operator=(FragTrap const & src)
{
    std::cout << "Assignation operator called" << std::endl;
    this->_Name = src.get_Name();
  	this->Max_hit_points = src.getMax_hit_points();
  	this->Max_energy_points = src.getMax_energy_points();
  	this->Ranged_attack_damage = src.getRanged_attack_damage();
  	this->Melee_attack_damage = src.getMelee_attack_damage();
  	this->Hit_points = src.getHitPoints();
  	this->Energy_points = src.getEnergyPoints();
  	this->Level = src.getLevel();
  	this->Armor_damage_reduction = src.getArmorDamageReduction();
    return *this;
}

std::string		FragTrap::get_Name() const {return this->_Name;}
int				FragTrap::getMax_hit_points() const {return this->Max_hit_points;}
int				FragTrap::getHitPoints() const {return this->Hit_points;}
int				FragTrap::getMax_energy_points() const {return this->Max_energy_points;}
int				FragTrap::getRanged_attack_damage() const {return this->Ranged_attack_damage;}
int				FragTrap::getMelee_attack_damage() const {return this->Melee_attack_damage;}
int				FragTrap::getEnergyPoints() const {return this->Energy_points;}
int				FragTrap::getLevel() const {return this->Level;}
int				FragTrap::getArmorDamageReduction() const {return this->Armor_damage_reduction;}

void	FragTrap::rangedAttack(std::string const & target)
{
	std::cout << this->_Name << " attacks " << target << " at range, causing " << this->Ranged_attack_damage << " points of damage !" << std::endl;
}

void	FragTrap::meleeAttack(std::string const & target)
{
	std::cout << this->_Name << " attacks " << target << " at range, causing " << this->Melee_attack_damage << " points of damage !" << std::endl;
}

void	FragTrap::trololo(std::string const & target)
{
	std::cout << this->_Name << " trololo attacks " << target << " at range, causing " << this->Ranged_attack_damage << " points of damage !" << std::endl;
}

void	FragTrap::unicorn(std::string const & target)
{
	std::cout << this->_Name << " unicorn attacks " << target << " at range, causing " << this->Melee_attack_damage << " points of damage !" << std::endl;
}

void	FragTrap::pony(std::string const & target)
{
	std::cout << this->_Name << " pony attacks " << target << " at range, causing " << this->Melee_attack_damage << " points of damage !" << std::endl;
}

void	FragTrap::setHitPoints(unsigned int amount) {this->Hit_points = amount;}

void 	FragTrap::takeDamage(unsigned int amount)
{

	int _amount = amount;

	if (_amount <= this->Armor_damage_reduction) {_amount = 5;}

	int result = getHitPoints() - (_amount - this->Armor_damage_reduction);

	if (result < 0) {result = 0;}

	setHitPoints(result);

	std::cout << this->Hit_points << std::endl;
}

void	FragTrap::beRepaired(unsigned int amount)
{
	int result = getHitPoints() + amount;

	if (result >= this->Max_hit_points) {result = this->Max_hit_points;};

	setHitPoints(result);

	std::cout << this->Hit_points << std::endl;
}


void	FragTrap::setEnergyPoints(unsigned int amount) {this->Energy_points = amount;}

int		FragTrap::random_int_in_range( void )
{
  	return ((rand() % 5 + 1));
}

void	FragTrap::variant( std::string const & target )
{
	int variant = random_int_in_range();

	if (variant == 1)
		rangedAttack(target);
	else if (variant == 2)
		meleeAttack(target);
	else if (variant == 3)
		trololo(target);
	else if (variant == 4)
		unicorn(target);
	else if (variant == 5)
		pony(target);
}

void	FragTrap::vaulthunter_dot_exe(std::string const & target)
{
	int result = getEnergyPoints() - 25;

	if (result < 0) result = 0;

	if (result <= 0)
		std::cout << "Oooops. Looks like no energy points left :(((" << std::endl;
	else
		variant(target);

	setEnergyPoints(result);
}

