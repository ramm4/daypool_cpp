#include "ScavTrap.hpp"

ScavTrap::ScavTrap() {}

ScavTrap::~ScavTrap()
{
	std::cout << "\033[1;33mDestruction:\033[0m" << std::endl;
	std::cout << "Killed bot with Name -> " << __Name << std::endl;
	std::cout << "Killed bot with MaxHitPoints -> " << MaxHitPoints << std::endl;
	std::cout << "Killed bot with HitPoints -> " << HitPoints << std::endl;
	std::cout << "Killed bot with MaxEnergyPoints -> " << MaxEnergyPoints << std::endl;
	std::cout << "Killed bot with RangedAttackDamage -> " << RangedAttackDamage << std::endl;
	std::cout << "Killed bot with MeleeAttackDamage -> " << MeleeAttackDamage << std::endl;
	std::cout << "Killed bot with EnergyPoints -> " << EnergyPoints << std::endl;
	std::cout << "Killed bot with _Level -> " << _Level << std::endl;
	std::cout << "Killed bot with ArmorDamageReduction -> " << ArmorDamageReduction << std::endl;
}

ScavTrap::ScavTrap(std::string const & Name): __Name(Name) 
{
	std::cout << "\033[1;33mConstruction:\033[0m" << std::endl;
	this->MaxHitPoints = 100;
	this->HitPoints = 100;
	this->MaxEnergyPoints = 50;
	this->RangedAttackDamage = 15;
	this->MeleeAttackDamage = 20;
	this->EnergyPoints = 50;
	this->_Level = 1;
	this->ArmorDamageReduction = 3;

	std::cout << "Initialized bot with Name -> " << __Name << std::endl;
	std::cout << "Initialized bot with MaxHitPoints -> " << MaxHitPoints << std::endl;
	std::cout << "Initialized bot with HitPoints -> " << HitPoints << std::endl;
	std::cout << "Initialized bot with MaxEnergyPoints -> " << MaxEnergyPoints << std::endl;
	std::cout << "Initialized bot with RangedAttackDamage -> " << RangedAttackDamage << std::endl;
	std::cout << "Initialized bot with MeleeAttackDamage -> " << MeleeAttackDamage << std::endl;
	std::cout << "Initialized bot with EnergyPoints -> " << EnergyPoints << std::endl;
	std::cout << "Initialized bot with _Level -> " << _Level << std::endl;
	std::cout << "Initialized bot with ArmorDamageReduction -> " << ArmorDamageReduction << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &src) 
{
    std::cout << "Copy Constructor called" << std::endl;
    *this = src;
}
ScavTrap &ScavTrap::operator=(ScavTrap const & src)
{
    std::cout << "Assignation operator called" << std::endl;
    this->__Name = src.get__Name();
  	this->MaxHitPoints = src.getMaxHitPoints();
  	this->MaxEnergyPoints = src.getMaxEnergyPoints();
  	this->RangedAttackDamage = src.getRangedAttackDamage();
  	this->MeleeAttackDamage = src.getMeleeAttackDamage();
  	this->HitPoints = src.getHitPoints();
  	this->EnergyPoints = src.getEnergyPoints();
  	this->_Level = src.get_Level();
  	this->ArmorDamageReduction = src.getArmorDamageReduction();
    return *this;
}

std::string		ScavTrap::get__Name() const {return this->__Name;}
int				ScavTrap::getMaxHitPoints() const {return this->MaxHitPoints;}
int				ScavTrap::getHitPoints() const {return this->HitPoints;}
int				ScavTrap::getMaxEnergyPoints() const {return this->MaxEnergyPoints;}
int				ScavTrap::getRangedAttackDamage() const {return this->RangedAttackDamage;}
int				ScavTrap::getMeleeAttackDamage() const {return this->MeleeAttackDamage;}
int				ScavTrap::getEnergyPoints() const {return this->EnergyPoints;}
int				ScavTrap::get_Level() const {return this->_Level;}
int				ScavTrap::getArmorDamageReduction() const {return this->ArmorDamageReduction;}

void	ScavTrap::rangedAttack(std::string const & target)
{
	std::cout << this->__Name << " attacks " << target << " at range, causing " << this->RangedAttackDamage << " points of Lannisters !" << std::endl;
}

void	ScavTrap::meleeAttack(std::string const & target)
{
	std::cout << this->__Name << " attacks " << target << " at range, causing " << this->MeleeAttackDamage << " points of Greyjoy !" << std::endl;
}

void	ScavTrap::trololo(std::string const & target)
{
	std::cout << this->__Name << " trololo attacks " << target << " at range, causing " << this->RangedAttackDamage << " points of Starks !" << std::endl;
}

void	ScavTrap::unicorn(std::string const & target)
{
	std::cout << this->__Name << " unicorn attacks " << target << " at range, causing " << this->MeleeAttackDamage << " points of Empire !" << std::endl;
}

void	ScavTrap::pony(std::string const & target)
{
	std::cout << this->__Name << " pony attacks " << target << " at range, causing " << this->MeleeAttackDamage << " points of Evil !" << std::endl;
}

void	ScavTrap::setHitPoints(unsigned int amount) {this->HitPoints = amount;}

void 	ScavTrap::takeDamage(unsigned int amount)
{

	int _amount = amount;

	if (_amount <= this->ArmorDamageReduction) {_amount = 5;}

	int result = getHitPoints() - (_amount - this->ArmorDamageReduction);

	if (result < 0) {result = 0;}

	setHitPoints(result);

	std::cout << this->HitPoints << std::endl;
}

void	ScavTrap::beRepaired(unsigned int amount)
{
	int result = getHitPoints() + amount;

	if (result >= this->MaxHitPoints) {result = this->MaxHitPoints;};

	setHitPoints(result);

	std::cout << this->HitPoints << std::endl;
}


void	ScavTrap::setEnergyPoints(unsigned int amount) {this->EnergyPoints = amount;}

int		ScavTrap::random_int_in_range( void )
{
  	return ((rand() % 5 + 1));
}

void	ScavTrap::variant( std::string const & target )
{
	int variant = random_int_in_range();

	if (variant == 1)
		rangedAttack(target);
	else if (variant == 2)
		meleeAttack(target);
	else if (variant == 3)
		trololo(target);
	else if (variant == 4)
		unicorn(target);
	else if (variant == 5)
		pony(target);
}

void	ScavTrap::challengeNewcomer(std::string const & target)
{
	int result = getEnergyPoints() - 25;

	if (result < 0) result = 0;

	if (result <= 0)
		std::cout << "Oooops. Looks like no energy points left :(((" << std::endl;
	else
		variant(target);

	setEnergyPoints(result);
}
