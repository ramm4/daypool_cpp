#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include <iostream>

class ScavTrap
{

private:
	std::string __Name;
	int MaxHitPoints;
	int MaxEnergyPoints;
	int RangedAttackDamage;
	int MeleeAttackDamage;
	int HitPoints;
	int EnergyPoints;
	int _Level;
	int ArmorDamageReduction;

public:
	ScavTrap();
	ScavTrap(std::string const & Name);
	~ScavTrap();
	ScavTrap(ScavTrap const &src);
	ScavTrap & operator=(ScavTrap const &src);
	
	std::string		get__Name() const;
	int		getMaxHitPoints() const;;
	int		getMaxEnergyPoints() const;
	int		getRangedAttackDamage() const;
	int		getMeleeAttackDamage() const;
	int		getHitPoints() const;
	int		getEnergyPoints() const;
	int		get_Level() const;
	int 	getArmorDamageReduction() const;

	void	setHitPoints(unsigned int amount);
	void	setEnergyPoints(unsigned int amount);

	void	rangedAttack(std::string const & target);
	void	meleeAttack(std::string const & target);

	void	trololo(std::string const & target);
	void	unicorn(std::string const & target);
	void	pony(std::string const & target);

	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	void	variant( std::string const & target );
	int		random_int_in_range( void );
	void	challengeNewcomer(std::string const & target);
	
};


#endif




