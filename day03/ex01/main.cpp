#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int		main(void)
{
	FragTrap Destroyer("Destroyer");

	// string destruction;
	std::cout << "\033[1;33mAttacks:\033[0m" << std::endl;
	
	Destroyer.rangedAttack("target");
		Destroyer.meleeAttack("target");

	// actual damage;
	std::cout << "\033[1;33mDamage func:\033[0m" << std::endl;
	
	Destroyer.takeDamage(-10000);
		Destroyer.takeDamage(20);
			Destroyer.takeDamage(10);
				Destroyer.takeDamage(1000);

	// healing section;
	std::cout << "\033[1;33mHealing:\033[0m" << std::endl;
	
	Destroyer.beRepaired(0);	
		Destroyer.beRepaired(1);	
			Destroyer.beRepaired(100000000);	


	// vaulthunter_dot_exe;
	std::cout << "\033[1;33mHunter:\033[0m" << std::endl;

	Destroyer.vaulthunter_dot_exe("HamBurger");	
		Destroyer.vaulthunter_dot_exe("CheeseBurger");
			Destroyer.vaulthunter_dot_exe("DoubleCheeseBurger");
				Destroyer.vaulthunter_dot_exe("Maffin");	
					Destroyer.vaulthunter_dot_exe("Rozshjok :)");	
						Destroyer.vaulthunter_dot_exe("HamBurger");	
							Destroyer.vaulthunter_dot_exe("CheeseBurger");
								Destroyer.vaulthunter_dot_exe("DoubleCheeseBurger");
									Destroyer.vaulthunter_dot_exe("Maffin");	
										Destroyer.vaulthunter_dot_exe("Rozshjok :)");	

	FragTrap Unicorn("Unicorn");
	FragTrap Killer("Killer");
	FragTrap Pony(Killer);
	Killer = Pony;

	ScavTrap I_hate_copy_past("I_hate_copy_past");

	// string destruction;
	std::cout << "\033[1;33mAttacks:\033[0m" << std::endl;
	
	I_hate_copy_past.rangedAttack("target");
		I_hate_copy_past.meleeAttack("target");

	// actual damage;
	std::cout << "\033[1;33mDamage func:\033[0m" << std::endl;
	
	I_hate_copy_past.takeDamage(-10000);
		I_hate_copy_past.takeDamage(20);
			I_hate_copy_past.takeDamage(10);
				I_hate_copy_past.takeDamage(1000);

	// healing section;
	std::cout << "\033[1;33mHealing:\033[0m" << std::endl;
	
	I_hate_copy_past.beRepaired(0);	
		I_hate_copy_past.beRepaired(1);	
			I_hate_copy_past.beRepaired(100000000);	

			
	// challengeNewcomer;
	std::cout << "\033[1;33mHunter:\033[0m" << std::endl;

	I_hate_copy_past.challengeNewcomer("HamBurger");	
		I_hate_copy_past.challengeNewcomer("CheeseBurger");
			I_hate_copy_past.challengeNewcomer("DoubleCheeseBurger");
				I_hate_copy_past.challengeNewcomer("Maffin");	
					I_hate_copy_past.challengeNewcomer("Rozshjok :)");	
						I_hate_copy_past.challengeNewcomer("HamBurger");	
							I_hate_copy_past.challengeNewcomer("CheeseBurger");
								I_hate_copy_past.challengeNewcomer("DoubleCheeseBurger");
									I_hate_copy_past.challengeNewcomer("Maffin");	
										I_hate_copy_past.challengeNewcomer("Rozshjok :)");	

	ScavTrap I_hate_copy_past2("Unicorn");
	ScavTrap I_hate_copy_past3("Killer");
	ScavTrap I_hate_copy_past1(I_hate_copy_past3);
	I_hate_copy_past3 = I_hate_copy_past2;
	return (0);
}