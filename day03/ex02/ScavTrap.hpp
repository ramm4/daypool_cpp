#ifndef SCAVTRAP_H
# define SCAVTRAP_H

# include <string>
# include "ClapTrap.hpp"

class ScavTrap: public ClapTrap
{
	private:
		static int					_challenge_number;
		static std::string			_challenges[];

	public:
		ScavTrap(std::string name);
		~ScavTrap(void);
		ScavTrap(ScavTrap const &src);

		ScavTrap		&operator=(ScavTrap const &rhs);
		void			challengeNewcomer(std::string const &target);
};

#endif