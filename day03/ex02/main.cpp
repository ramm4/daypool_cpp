#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include <iostream>

int				main(void)
{
	std::cout << "\033[1;33mCreatio ex nihilo:\033[0m" << std::endl;

	ScavTrap	Palpatine("CheeseBurger");
	FragTrap	Enakin("DoubleCheeseBurger");

	std::cout << "\033[1;33mFight:\033[0m" << std::endl;	

	Palpatine.rangedAttack(Enakin.getName());
	Enakin.takeDamage(Palpatine.getRangedAttackDamage());

	Enakin.meleeAttack(Palpatine.getName());
	Palpatine.takeDamage(Enakin.getMeleeAttackDamage());

	Palpatine.meleeAttack(Enakin.getName());
	Enakin.takeDamage(Palpatine.getMeleeAttackDamage());

	Enakin.rangedAttack(Palpatine.getName());
	Palpatine.takeDamage(Enakin.getRangedAttackDamage());

	Enakin.beRepaired(-100);

	Enakin.rangedAttack(Palpatine.getName());
	Palpatine.takeDamage(Enakin.getRangedAttackDamage());


	std::cout << "\033[1;33mScavTrap.challangeNewComer method:\033[0m" << std::endl;

	Palpatine.challengeNewcomer(Enakin.getName());
	Palpatine.challengeNewcomer(Enakin.getName());
	Palpatine.challengeNewcomer(Enakin.getName());
	Palpatine.challengeNewcomer(Enakin.getName());
	Palpatine.challengeNewcomer(Enakin.getName());
	Palpatine.challengeNewcomer(Enakin.getName());

	std::cout << "\033[1;33mtakeDamage method:\033[0m" << std::endl;

	Palpatine.takeDamage(Enakin.vaulthunter_dot_exe(Palpatine.getName()));
	Palpatine.takeDamage(Enakin.vaulthunter_dot_exe(Palpatine.getName()));

	Enakin.takeDamage(1000000);

	std::cout << "\033[1;33mFikus:\033[0m" << std::endl;
}