#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>

#include "ClapTrap.hpp"
#include "NinjaTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

NinjaTrap::NinjaTrap(std::string name):
	ClapTrap(name, "Kill some Samurai", 60, 120, 60, 5, 0)
{
	srand(time(NULL));
	std::cout << "Ninja is somewhere around ..." << _name << std:: endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const &src):
	ClapTrap(src._name, "Kill some Samurai", 60, 120, 60, 5, 0)
{
	std::cout << "Booo" << _name << "Don`t mess with a Ninja" << std:: endl;
}

NinjaTrap::~NinjaTrap(void)
{
	std::cout << "NinjaTrap != ALIVe. Nooooooooooo !" << std::endl;
}

NinjaTrap		&NinjaTrap::operator=(NinjaTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}

void			NinjaTrap::ninjaShoebox(FragTrap &target) const
{
	std::cout << _name << " FragTrap kill !" << std::endl;
	target.takeDamage(rand() % 50);
}

void			NinjaTrap::ninjaShoebox(ScavTrap &target) const
{
	std::cout << _name << " ScavTrap kill !" << std::endl;
	target.takeDamage(rand() % 10);
}
