#ifndef NINJATRAP_H
# define NINJATRAP_H

# include <string>
# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class NinjaTrap: public ClapTrap
{
	public:
		NinjaTrap(std::string name);
		~NinjaTrap(void);
		NinjaTrap(NinjaTrap const &src);

		NinjaTrap		&operator=(NinjaTrap const &rhs);

		void			ninjaShoebox(FragTrap &target) const;
		void			ninjaShoebox(ScavTrap &target) const;
};

#endif