#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "FragTrap.hpp"

int						FragTrap::_attack_number = 5;

unsigned int (FragTrap::*FragTrap::_attacks[])(std::string const &target) = {

	&FragTrap::kill1,
	&FragTrap::kill2,
	&FragTrap::kill3,
	&FragTrap::kill4,
	&FragTrap::kill5
};

FragTrap::FragTrap(std::string name): ClapTrap(name, "FragTrap", 100, 100, 30, 20, 5)
{
	srand(time(NULL));
	std::cout << "BIOS ... " << std:: endl;
}

FragTrap::FragTrap(FragTrap const &src):
	ClapTrap(src._name, "FragTrap", 100, 100, 30, 20, 5)
{
	std::cout << "BIOS ... " << _name<< std:: endl;
}

FragTrap::~FragTrap(void)
{
	std::cout << "FragTrap is DEAD !" << std::endl;
}

FragTrap		&FragTrap::operator=(FragTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}

unsigned int	FragTrap::vaulthunter_dot_exe(std::string const &target)
{
	int			random;

	if (this->_energyPoints >= 25)
	{
		this->_energyPoints -= 25;
		random = rand() % _attack_number;
		return ((this->*_attacks[random])(target));
	}
	else
		std::cout << "Ups. No energy" << std::endl;
	return (0);
}

unsigned int	FragTrap::kill1(std::string const &target)
{
	std::cout << "FragTrap | " << this->_name << " | badaboom <" << target << " | " << std::endl;
	return (30);
}

unsigned int	FragTrap::kill2(std::string const &target)
{
	std::cout << "FragTrap | " << this->_name << " | iferno <" << target << " | " << std::endl;
	return (50);
}

unsigned int	FragTrap::kill3(std::string const &target)
{
	std::cout << "FragTrap |" << this->_name << "| unicorns attack <" << target << " | " << std::endl;
	return (50);
}

unsigned int	FragTrap::kill4(std::string const &target)
{
	std::cout << "FragTrap |" << this->_name << "| o.k. <" << target << "|, daisy stuff is quitie." << std::endl;
	return(15);
}

unsigned int	FragTrap::kill5(std::string const &target)
{
	std::cout << "FragTrap <" << this->_name<< "| kill <"<< target << "|, my barbeque finally"<< std::endl;
	return(70);
}