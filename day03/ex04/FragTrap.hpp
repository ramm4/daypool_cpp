#ifndef FRAGTRAP_H
# define FRAGTRAP_H

# include <string>
# include "ClapTrap.hpp"

class FragTrap: public ClapTrap
{
	private:
		static int						_attack_number;
		static unsigned int (FragTrap::*_attacks[])(std::string const &target);

	public:
		FragTrap(std::string name);
		~FragTrap(void);
		FragTrap(FragTrap const &src);

		FragTrap		&operator=(FragTrap const &rhs);

		unsigned int	vaulthunter_dot_exe(std::string const &target);

		unsigned int	kill1(std::string const &target);
		unsigned int	kill2(std::string const &target);
		unsigned int	kill3(std::string const &target);
		unsigned int	kill4(std::string const &target);
		unsigned int	kill5(std::string const &target);
};

#endif