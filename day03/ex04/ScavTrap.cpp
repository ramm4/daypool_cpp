
#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "ScavTrap.hpp"

int					ScavTrap::_challenge_number = 4;

std::string			ScavTrap::_challenges[] = {
	"Make perfect atoi during the Piscine of the C",
	"Get some Personal life with UNIT",
	"Trolololololo",
	"Try do all the tasks of d02"
};

ScavTrap::ScavTrap(std::string name):
	ClapTrap(name, "ScavTrap", 100, 50, 20, 15, 3)
{
	srand(time(NULL));
	std::cout << "It is alive -> " << _name << std:: endl;
}

ScavTrap::ScavTrap(ScavTrap const &src):
	ClapTrap(src._name, "ScavTrap", 100, 50, 20, 15, 3)
{
	std::cout << "It is alive -> " << _name << std:: endl;
}

ScavTrap::~ScavTrap(void)
{
	std::cout << "ScavTrap is totally dead" << std::endl;
}

ScavTrap		&ScavTrap::operator=(ScavTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}

void			ScavTrap::challengeNewcomer(std::string const &target)
{
	int			random;

	random = rand() % _challenge_number;
	std::cout << _challenges[random] << ' ' << target << std::endl;
}