
#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "SuperTrap.hpp"

SuperTrap::SuperTrap(std::string name): /*ClapTrap(name, "SuperTrap", 100, 120, 60, 20, 5), */FragTrap(name), NinjaTrap(name)
{
	srand(time(NULL));
	std::cout << "SuperTrap is alive" << std:: endl;
}

SuperTrap::SuperTrap(SuperTrap const &src): /*ClapTrap(src), */FragTrap(src), NinjaTrap(src)
{
	std::cout << "SuperTrap is alive" << std:: endl;
}

SuperTrap::~SuperTrap(void)
{
	std::cout << "SuperTrap has been destructed" << std::endl;
}

SuperTrap		&SuperTrap::operator=(SuperTrap const	&rhs)
{
	if (this != &rhs)
		SuperTrap::operator=(rhs);
	return *this;
}
