
#ifndef SUPERTRAP_H
# define SUPERTRAP_H

# include <string>
# include <iostream>
# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class SuperTrap: public virtual FragTrap, public virtual NinjaTrap
{
	public:
		
		SuperTrap(std::string name);
		~SuperTrap(void);
		SuperTrap(SuperTrap const &src);

		SuperTrap		&operator=(SuperTrap const &rhs);
};

#endif