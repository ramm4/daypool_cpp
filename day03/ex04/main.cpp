#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

#include <iostream>

int		main() {

	
	std::cout << "\033[1;33mCreatio ex nihilo:\033[0m" << std::endl;

	ScavTrap	Palpatine("CheeseBurger");
	FragTrap	Enakin("DoubleCheeseBurger");
	NinjaTrap	ninja("you did not see anythin : )))))))");
	SuperTrap	SuperTrap("Super");

	std::cout << "\033[1;33mSome war in Japan: act 1\033[0m" << std::endl;

	Palpatine.rangedAttack(Enakin.getName());
	Enakin.takeDamage(Palpatine.getRangedAttackDamage());

	Enakin.meleeAttack(Palpatine.getName());
	Palpatine.takeDamage(Enakin.getMeleeAttackDamage());

	Palpatine.meleeAttack(Enakin.getName());
	Enakin.takeDamage(Palpatine.getMeleeAttackDamage());

	Enakin.rangedAttack(Palpatine.getName());
	Palpatine.takeDamage(Enakin.getRangedAttackDamage());

	Enakin.beRepaired(50);

	Enakin.rangedAttack(Palpatine.getName());
	Palpatine.takeDamage(Enakin.getRangedAttackDamage());

	Palpatine.challengeNewcomer(ninja.getName());
	Palpatine.challengeNewcomer(ninja.getName());
	Palpatine.challengeNewcomer(ninja.getName());


	std::cout << "\033[1;33mSome war in Japan: act 2\033[0m" << std::endl;

	ninja.ninjaShoebox(Palpatine);
	ninja.ninjaShoebox(Palpatine);

	std::cout << "\033[1;33mSome war in Japan: act 4: EL MAHACH\033[0m" << std::endl;


	Palpatine.takeDamage(Enakin.vaulthunter_dot_exe(Palpatine.getName()));
	Palpatine.takeDamage(Enakin.vaulthunter_dot_exe(Palpatine.getName()));

	std::cout << "\033[1;33mSome war in Japan: act 5: ninjaShoebox\033[0m" << std::endl;

	ninja.ninjaShoebox(Enakin);
	ninja.ninjaShoebox(Enakin);

	std::cout << "\033[1;33mSome war in Japan: act 6: fatality\033[0m" << std::endl;

	Enakin.takeDamage(200);

	std::cout << "\033[1;33mSome war in Japan: act 7: SuperTrap\033[0m" << std::endl;

	SuperTrap.ninjaShoebox(Palpatine);
	SuperTrap.ninjaShoebox(Enakin);

	std::cout << "\033[1;33mFikus:\033[0m" << std::endl;
}
