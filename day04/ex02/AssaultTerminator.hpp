#ifndef ASSAULTTERMINATOR_H
# define ASSAULTTERMINATOR_H

# include <string>
# include "ISpaceMarine.hpp"

class AssaultTerminator: public ISpaceMarine
{
	public:
		AssaultTerminator(void);
		~AssaultTerminator(void);
		AssaultTerminator(AssaultTerminator const &src);

		AssaultTerminator	&operator=(AssaultTerminator const &rhs);

		ISpaceMarine		*clone() const;
		void				battleCry() const;
		void				rangedAttack() const;
		void				meleeAttack() const;
};

#endif
