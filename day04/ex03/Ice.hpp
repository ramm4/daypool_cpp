#ifndef ICE_H
# define ICE_H

# include <string>
# include "ICharacter.hpp"

class Ice: public AMateria
{
	public:
		Ice(void);
		~Ice(void);
		Ice(Ice const &src);

		Ice					&operator=(Ice const &rhs);

		AMateria			*clone() const;
		void				use(ICharacter const &target);
};

#endif
