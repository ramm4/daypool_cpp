#ifndef MATERIASOURCE_H
# define MATERIASOURCE_H

# include <string>
# include "IMateriaSource.hpp"

class MateriaSource: public IMateriaSource
{
	private:
		int					_materia_count;
		AMateria			*_materias[4];

	public:
		MateriaSource(void);
		~MateriaSource(void);
		MateriaSource(MateriaSource const &src);

		MateriaSource		&operator=(MateriaSource const &rhs);

		void				learnMateria(AMateria *materia);
		AMateria			*createMateria(std::string const &type);
};

#endif
