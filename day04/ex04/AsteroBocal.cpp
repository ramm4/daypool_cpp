#include <iostream>
#include <string>
#include "AsteroBocal.hpp"

AsteroBocal::AsteroBocal(void)
{}

AsteroBocal::AsteroBocal(AsteroBocal const &src)
{
	*this = src;
}

AsteroBocal::~AsteroBocal(void)
{}

AsteroBocal			&AsteroBocal::operator=(AsteroBocal const &rhs)
{
	(void)rhs;
	return (*this);
}


std::string				AsteroBocal::beMined(DeepCoreMiner *laser) const
{
	(void)laser;
	return ("Thorite");
}

std::string				AsteroBocal::beMined(StripMiner *laser) const
{
	(void)laser;
	return ("Flavium");
}

std::string	const		AsteroBocal::getName(void) const
{
	return ("AsteroBocal");
}
