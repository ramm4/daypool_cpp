#ifndef BOCALSTEROID_H
# define BOCALSTEROID_H

# include <string>
# include "IAsteroid.hpp"

class BocalSteroid: public IAsteroid
{
	public:
		BocalSteroid(void);
		~BocalSteroid(void);
		BocalSteroid(BocalSteroid const &src);

		BocalSteroid			&operator=(BocalSteroid const &rhs);

		std::string				beMined(DeepCoreMiner *laser) const;
		std::string				beMined(StripMiner *laser) const;

		std::string	const		getName(void) const;
};

#endif
