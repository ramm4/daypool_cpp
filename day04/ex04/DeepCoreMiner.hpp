#ifndef DEEPCOREMINER_H
# define DEEPCOREMINER_H

# include <string>
# include "IMiningLaser.hpp"

class DeepCoreMiner: public IMiningLaser
{
	public:
		DeepCoreMiner(void);
		~DeepCoreMiner(void);
		DeepCoreMiner(DeepCoreMiner const &src);

		DeepCoreMiner			&operator=(DeepCoreMiner const &rhs);

		void					mine(IAsteroid *asteroid);
};

#endif
