#ifndef IMININGLASER_H
# define IMININGLASER_H

# include <string>
# include "IAsteroid.hpp"

class IAsteroid;

class IMiningLaser
{
	public:
		virtual ~IMiningLaser() {}

		virtual void			mine(IAsteroid *asteroid) = 0;
};

#endif
