#ifndef STRIPMINER_H
# define STRIPMINER_H

# include <string>
# include "IMiningLaser.hpp"

class StripMiner: public IMiningLaser
{
	public:
		StripMiner(void);
		~StripMiner(void);
		StripMiner(StripMiner const &src);

		StripMiner				&operator=(StripMiner const &rhs);

		void					mine(IAsteroid *asteroid);
};

#endif
