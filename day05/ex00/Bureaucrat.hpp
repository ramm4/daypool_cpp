#ifndef BUREAUCRAT_HPP_
# define BUREAUCRAT_HPP_

#include <iostream>

class Bureaucrat
{
	private:
		int			_grade;
		std::string	_name;
	public:
		Bureaucrat();
		Bureaucrat(std::string name, int grade);

		class GradeTooHighException: public std::exception
		{
			public:
				GradeTooHighException();
				GradeTooHighException(GradeTooHighException const &);
				~GradeTooHighException() throw();
				GradeTooHighException &		operator=(GradeTooHighException const &);
				virtual const char* what() const throw();
		};

		class GradeTooLowException: public std::exception
		{
			public:
				GradeTooLowException();
				GradeTooLowException(GradeTooLowException const &);
				~GradeTooLowException() throw();
				GradeTooLowException &		operator=(GradeTooLowException const &);
				virtual const char* what() const throw();
		};

		std::string	getName() const;
		int			getGrade() const;
		void		operator-=(int const num);
		void		operator+=(int const num);

};

std::ostream &operator<<(std::ostream &, Bureaucrat const &);

#endif