#include "Bureaucrat.hpp"

int main(void)
{
	Bureaucrat person1 = Bureaucrat( "Petbka", 150);
	std::cout << person1;
	Bureaucrat person2 = Bureaucrat( "Volodbka", 99);
	std::cout << person2;
	Bureaucrat person3 = Bureaucrat( "Bacbka", 1);
	std::cout << person3;

	try
	{
		int grade = 100;
		std::cout << std::endl << "---> Adding "<<grade<<" grade to " << person1.getName() << std::endl;
		person1 += grade;
		std::cout << person1;
	}
	catch (Bureaucrat::GradeTooHighException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		int grade = 15;
		std::cout << std::endl << "---> Degrading "<<grade<<" grade to " << person2.getName() << std::endl;
		person2 -= grade;
		std::cout << person2;
	}
	catch (Bureaucrat::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		int grade = 1;
		std::cout << std::endl << "---> Adding "<<grade<<" grade to " << person3.getName() << std::endl;
		person3 += grade;
		std::cout << person3;
	}
	catch (Bureaucrat::GradeTooHighException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		int grade = 5;
		std::cout << std::endl << "---> Trying to instanciate a bureaucrat grade " <<grade << std::endl;
		Bureaucrat person4 = Bureaucrat("Phillip Kirkorov", grade);
		std::cout << person4;
	}
	catch (Bureaucrat::GradeTooHighException e){
		std::cout << e.what() << std::endl;
	}

	try
	{
		int grade = 155;
		std::cout << std::endl << "---> Trying to instanciate a bureaucrat grade " <<grade << std::endl;
		Bureaucrat person5 = Bureaucrat("Dimon", grade);
		std::cout << person5;
	}
	catch (Bureaucrat::GradeTooLowException e){
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "---> Instancying copy from Volodbka" << std::endl;
	Bureaucrat person6(person2);
	std::cout << person6;

	std::cout << std::endl << "************ Forms ************" << std::endl;
    
    Form Form1 = Form("Billing", 110, 40);
    std::cout << Form1;
    Form Form2(Form1);
    std::cout << Form2;
    Form Form3 = Form();
    std::cout << Form3;
    try
    {
        Buro1.signForm(Form1);
    }
    catch (Form::GradeTooLowException e)
    {
        std::cout << e.what() << std::endl;
    }
    try
    {
 	   Form3.beSigned(Buro2);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}
	try
	{
	    Form2.beSigned(Buro3);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	return 0;
}