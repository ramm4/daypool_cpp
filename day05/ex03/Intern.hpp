

#ifndef Intern_HPP
# define Intern_HPP

#include <iostream>
#include "Form.hpp"

class Form;

class Intern 
{

public:
	Intern(void);
	Intern(Intern const & src);
	~Intern(void);
	Intern & operator=( Intern const &);

	Form * makeForm(std::string form, std::string target);
};

#endif
