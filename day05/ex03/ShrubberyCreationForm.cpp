

#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <fstream>

ShrubberyCreationForm::ShrubberyCreationForm( void ) : Form()
{

}

ShrubberyCreationForm::ShrubberyCreationForm( std::string target ) : Form()
{
	this->setTarget(target);
}

ShrubberyCreationForm::ShrubberyCreationForm( ShrubberyCreationForm const & src ) : Form()
{
	*this = src;
}

ShrubberyCreationForm::~ShrubberyCreationForm( void )
{

}

ShrubberyCreationForm & ShrubberyCreationForm::operator=( ShrubberyCreationForm const & rhs )
{
	if (this != &rhs)
		return *this;
	return *this;
}

void ShrubberyCreationForm::execute(Bureaucrat const & executor) const
{
	std::string name = this->getTarget() + "_shrubbery";
	const char *fileName = name.c_str();
	if (executor.getGrade() <= 137 && this->getSign() == true)
	{
		std::ofstream ofs(fileName);
		if (ofs)
		{
			ofs	<< "********************" << std::endl
				<< "*       |_:_|      *" << std::endl
				<< "*      /(_Y_)\\     *" << std::endl
				<< "*     ( \\/M\\// )   *" << std::endl
				<< "*   _.'-/'-'\\-'._  *" << std::endl
				<< "* _/.--'[[[[]'--.\\_*" << std::endl
				<< "*                  *" << std::endl
				<< "*  				   *" << std::endl
				<< "********************" << std::endl
				<< std::endl;
		}
		ofs.close();
	}
	else if (this->getSign() == false)
		std::cout << "The form isn't signed yet." << std::endl;
	else
		throw Form::GradeTooLowException();
}