

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <cstdlib>

int main( void )
{
	std::cout << "\t\t\t Intern Creation \t\t\t" << std::endl << std::endl;

	Intern someRandomIntern;

	Form* rrf;
	Form* scf;
	Form* ppf;
	
	rrf = someRandomIntern.makeForm("robotomy request", "SH*T");
	std::cout << "Target: " << rrf->getTarget() << std::endl;
	scf = someRandomIntern.makeForm("shrubbery creation", "F*CK");
	std::cout << "Target: " << scf->getTarget() << std::endl;
	ppf = someRandomIntern.makeForm("presidential pardon", "LUSUY");
	std::cout << "Target: " << ppf->getTarget() << std::endl;

	delete rrf;
	delete scf;
	delete ppf;

	return (0);
}
