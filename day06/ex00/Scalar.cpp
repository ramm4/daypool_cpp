#include "Scalar.hpp"

ScalarConverter::ScalarConverter() : _value("") {}

ScalarConverter::ScalarConverter(std::string val) : _value(val) {}

ScalarConverter::ScalarConverter(const ScalarConverter &obj)
{
	*this = obj;
}

ScalarConverter::~ScalarConverter(void) {}

std::string		ScalarConverter::get_value(void) const
{
	return(this->_value);
}

ScalarConverter &ScalarConverter::operator=(ScalarConverter const &src)
{
	_value = src.get_value();
	return (*this);
}

std::ostream	&operator<<(std::ostream &new_os, const ScalarConverter &obj)
{
	new_os<<static_cast<std::string const &>(obj);
	return (new_os);
}

ScalarConverter::NoConversationException::NoConversationException(void) {}
ScalarConverter::NoConversationException::~NoConversationException(void) throw() {}
char const *ScalarConverter::NoConversationException::what(void) const throw()
{
	return ("No conversion");
}

	ScalarConverter::operator std::string const &(void) const
{
	return (_value);
}

	ScalarConverter::operator int(void) const
{
	int x;
		try
		{
			x = std::stoi(_value, nullptr, 10);
		}
		catch(...)
		{
			throw(NoConversationException());
		}
		return (x);
}

	ScalarConverter::operator char(void) const
{
	char x;
		try
		{
			x = static_cast<char>(std::stoi(_value, nullptr, 10));
		}
		catch(...)
		{
			throw(NoConversationException());
		}
		return (x);
}

	ScalarConverter::operator float(void) const
{
	float x;
		try
		{
			x = (std::stof(_value, nullptr));
		}
		catch(...)
		{
			throw(NoConversationException());
		}
		return (x);
}

	ScalarConverter::operator double(void) const
{
	double x;
		try
		{
			x = (std::stod(_value, nullptr));
		}
		catch(...)
		{
			throw(NoConversationException());
		}
		return (x);
}