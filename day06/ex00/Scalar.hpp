#ifndef SCALAR
# define SCALAR

#include <iostream>
#include <stdexcept>

class ScalarConverter
{
	private:
		std::string _value;

	public:
	ScalarConverter(void);
	ScalarConverter(std::string);
	ScalarConverter(const ScalarConverter &);
	~ScalarConverter(void);
	std::string get_value(void) const;

	class NoConversationException : public std::exception
	{

		public:
			NoConversationException(void);
			~NoConversationException(void) throw();

			char const 	*what(void) const throw();
	};

	operator int(void) const;
	operator float(void) const;
	operator double(void) const;
	operator char(void) const;
	operator std::string const &(void) const;
	ScalarConverter &operator=(ScalarConverter const &);
};

std::ostream &operator<<(std::ostream &new_os, const ScalarConverter &obj);

#endif