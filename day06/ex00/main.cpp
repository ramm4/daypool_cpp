#include "Scalar.hpp"
#include <iomanip>

int main(int argc, char **argv)
{
	if (argc != 2)
		{
			std::cout << "Please, input the parameter."<<std::endl;
			return (0);
		}

	ScalarConverter value(argv[1]);

	std::cout << "char: ";
	try {
		char c = static_cast<char>(value);
		if (isprint(c))
			std::cout << '\'' << c << '\'';
		else
			std::cout << "Non displayable";
	}
	catch (std::exception) {
		std::cout << "impossible";
	}
	std::cout << std::endl;

	std::cout << "int: ";

	try {
		std::cout << static_cast<int>(value);
	}
	catch (std::exception) {
		std::cout << "impossible";
	}
	std::cout << std::endl;

	std::cout << std::fixed;
	std::cout.precision(1);

	std::cout << "float: ";
	try {
		std::cout << static_cast<float>(value) << 'f';
	}
	catch (std::exception) {
		std::cout << "impossible";
	}
	std::cout << std::endl;


	std::cout << "double: ";
	try {
		std::cout << static_cast<double>(value);
	}
	catch (std::exception) {
		std::cout << "impossible";
	}
	std::cout << std::endl;

	return (0);
}