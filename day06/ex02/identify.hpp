

#ifndef IDENTIFY_H
# define IDENTIFY_H

# include "Base.hpp"

Base			*generate(void);
void			identify_from_pointer(Base *p);
void			identify_from_reference(Base &p);

#endif
