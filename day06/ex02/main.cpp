

#include "identify.hpp"
#include "Base.hpp"

int					main(void)
{
	Base			*b = generate();

	identify_from_pointer(b);
	identify_from_reference(*b);
	return (0);
}
