#include <string>
#include <iostream>

#define size 5

template <typename T>

void			iter(T *array, size_t len, void (*func)(T &))
{
	size_t		i;

	i = 0;
	while (i < len)
	{
		(*func)(array[i]);
		i++;
	}
	std::cout << std::endl;
}

template <typename T>

void			print(T &data)
{
	std::cout << data << std::endl;
}

int main( void )
{
	int				array_ints[] = {1, 2, 3, 4, 5 };
	
	std::string		array_strings[] = { "If", "you", "decide", "to leave me", "I'm ", "going", "with", "you!"};

	iter<int>(array_ints, sizeof(array_ints)/sizeof(array_ints[0]), &print<int>);

	iter<std::string>(array_strings, sizeof(array_strings)/sizeof(array_strings[0]), &print<std::string>);

	return (0);
}
