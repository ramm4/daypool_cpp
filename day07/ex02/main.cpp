#include <string>
#include <iostream>
#include <exception>
#include "Array.hpp"

int				main(void)
{
	Array<int>		array1(20);
	Array<int>		array2;

	std::cout << "First array size: " << array2.size() << std::endl;
	std::cout << "Empty array size: " << array1.size() << std::endl;

	array1[6] = 100;
	std::cout << "array1[6] : " << array1[6] << std::endl;
	array1[9] = 1;
	std::cout << "array1[9] : " << array1[9] << std::endl;

	array2 = array1;
	std::cout << "array1 in array2..." << std::endl;

	std::cout << "array2[6] : " << array2[6] << std::endl;
	std::cout << "array2[9] : " << array2[9] << std::endl;

	std::cout << "set to array1[30]" << std::endl;
	try {
		array1[30] = 42;
	}
	catch (std::exception &e) {
		std::cout << "ERROR (expected): " << e.what() << std::endl;
	}

	return (0);
}
