#include <iostream> 

int min(int x, int y)
{
	return (x > y ? y : x);
}

void merge(int *arr, int l, int m, int r)
{
	int i, j, k; 
    int n1 = m - l + 1; 
    int n2 =  r - m; 

	int L[n1], R[n2];

	for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1+ j];

	i = 0; 
    j = 0; 
    k = l;

	while (i < n1 && j < n2) 
    { 
        if (L[i] <= R[j]) 
        { 
            arr[k] = L[i]; 
            i++; 
        } 
        else
        { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    }

	/* Copy the remaining elements of L[], if there are any */
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    }

    /* Copy the remaining elements of R[], if there are any */
    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
}

void merge_sort(int *array, size_t n)
{
	int	curr_size;
	int left_start;

	for (curr_size = 1; curr_size < n - 1; curr_size *= 2)
	{
		for (left_start = 0; left_start < n - 1; left_start += 2 * curr_size)
		{
			int mid = left_start + curr_size - 1;
			int right_end = min(left_start + 2*curr_size - 1, n - 1);

			merge(array, left_start, mid, right_end);
		}
	}
}

int	main(void)
{
	int array[] = {4,5,1,258,66,75,12,8,65,4,87,63,53,8,99,54,12,34};

	for (auto v : array)
	{
		std::cout<<v<<" ";
	}
	std::cout<<std::endl;

	merge_sort(array, sizeof(array)/sizeof(array[0]));

	for    (auto v : array)
    {
		std::cout<<v<<" ";
    }
	std::cout<<std::endl;

	return 0;
}
